# Public Research Vault
Hello there, and welcome to the public research vault which contains the majority of the knowledge learned. 

## What is this?
Well, it's a vault of all the knowledge that I've collected while researching the respective topics found in there (e.g. CI/CD and Kubernetes development). 

I decided to gather as much information as I could here and then share it with you, as I see a lot of misleading information (or none at all) on some of the topics found in here, so that we could easily combat this. 

## How does the vault work?
The vault is made using a free open-source notetaking application called [Obisidian](https://obsidian.md/). This tool allows for quick overview of the notes and I therefore highly recommend that you download this repository and the [Obisidian](https://obsidian.md/) application for the most optimal reading experience. 

All the files written in this vault is done in Markdown, which ultimately means that you can always just read information gathered here in any Markdown reader of your choosing (I do however still recommend that you try and read it in Obisidian first). 

## Table of Content
- APIs
	- Cloud Foundry API
	- SharePoint API
	- SuccessFactors API
- CI/CD
	- Continuous Deployment
		- Continuous Cloud Foundry Deployment
		- Continuous SAP Neo Deployment
	- Continuous Integration
		- API Testing
		- End2End Testing
		- Integration Testing
		- Frameworks & Tools
- Cloud Development
	- Cloud Foundry
	- Docker
	- Kubernetes
	- Kyma
	- Redis
	- Terraform
- Frontend Developtment
	- Angular
	- React
	- UI5
	- Vue.js

***WITH MORE TO COME! ***

## Can I contribute?
Yes! Of course! The more information that we can gather the better, as it will eventually help all of us get better in the end. 

### Okay, so how do I contribute?
Open up [Obisidian](https://obsidian.md/), start writing, and once you're satisfied with what you've written down you simply perform a neat little pull-request to this repository.

The pull-request structure is here to make sure that we don't get redundant or misleading information in the vault.

### Contribution Rules
***WORK IN PROGRESS***

