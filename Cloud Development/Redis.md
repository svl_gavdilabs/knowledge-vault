# Redis
*WORK IN PROGRESS*

**Table of Contents**
- [[#Basics]]
	- [[#Master-Slave Setup Cluster]]
	- [[#Alternative Master-Slave Setup Sentinel]]
- [[#Cluster Deployment]]
	- [[#Prerequisites]]
	- [[#Configuring Your Cluster]]

## Basics
Redis is an open source in-memory database, which can function as a key-value database, cache and message broker. 

Primarily Redis is  used as memory cache in cloud structures to ease the load on the database. 

Redis clusters, in a  [[Kubernetes]] context, is designed to scale your database storage by partitioning it which in turn makes it more resilient. 

### Master-Slave Setup (Cluster)
Each member of a Redis cluster, whether it is a primary or secondary replica, manages a subset of the hash slot. 

If a master node becomes unreachable, a slave node will take its place to ensure that there is no downtime. 

![[redis_master_slave.png]]

Minimal setups of a Redis cluster must be made up of atleast three master nodes and three slave nodes. The rule of thumb here being that foreach master node there must always be a slave node. 

**Each master node in a cluster will be assigned a hash slot range between 0 and 16,383. **

Let's say we made a minimal setup of 3x3, then the master node A contains hash slots from 0 to 5000, master node B contains 5001 to 10000, and master node C from 10001 to 16383. 

Master nodes are in charge of writing data and the  slave nodes will assist in handling read requests.

Communication between these nodes is then done using an internal message bus, using a gossip protocol to propagate the information about the cluster, or even to discover new nodes.

### Alternative Master-Slave Setup (Sentinel)
An alternative to the cluster setup from the previous section is the Redis Sentinel setup. This setup consists of one master node connected to several slave nodes. These slave nodes replicates the master node. 

Much like the cluster setup, if the master node goes down one of the slave nodes will take its place. Once a master node has been replaced, the downed node will then get rebooted as slave node and mirror the new master node. 

The big downside to doing this setup compared to the cluster setup is that there is only one node that handles write commands. 

**ONLY USE SENTINEL IF YOU DO NOT REQUIRE HEAVY/INTENSE WRITING TO YOUR REDIS INSTANCE**

## Cluster Deployment
Based on the [[#Master-Slave Setup Cluster]] we can setup a Redis cluster that is ready for use. This will most likely be done in connection with a [[Kubernetes]] instance.

Currently there are solutions out there that handle the deployment and maintance of a Redis cluster, like Rancher, however these solutions usually comes at a price. Therefore these docs here will only focus on how to setup and maintain a vanilla instance. 

### Prerequisites
As with [[Kubernetes]], it is important that you have the ```kubectl``` CLI tool installed. Without this tool you will not be able to deploy or test your Redis cluster, atleast not with the approach being taking here. 

### Configuring Your Cluster


***TBC***