# Terraform
*Work in Progress*

**Table of Contents**
- [[#Basics]]
	- [[#First Steps]]
	- [[#Building Your Infrastructure]]
- [[#Terraform Language]]
- [[#Provisioning AKS Cluster]]

## Basics
Terraform is a solution for delivering infrastructure as code, and is a competitor (somewhat) to Ansible.

Terraform makes use of its own language for configuration and therefore all files pertaining to your Terraform setup should have the `.tf`extension.

For more on the language itself, have a look at the [[#Terraform Language]] section.

### First Steps
he absolute first step when working with Terraform for one of your projects is to make sure that you have the CLI tool installed. The Terraform can be installed through package managers such as Chocolately (Windows) and Homebrew (MacOS).

**Windows**  
`choco install terraform`

**MacOS**  
`brew install terraform`

Once the installation process is over you should be ready to use Terraform with any of your projects.

The next step from that point on will then be to authenticate your Terraform CLI with the desired deployment platform. These can vary from AWS EC2, Microsoft Azure, Google Cloud Platform, to a local Linux instance.

If you decide to configure with an external vendor, it is important that you have their respective CLI tools installed as well, in my case I'm using Terraform with Microsoft Azure and therefore I have their CLI tool installed.

Once you're certain that you have all necessary CLI tools installed and authenticated you're then ready to start building your platform infrastructure

### Building Your Infrastructure
When preparing for your first infrastructure build, the first file you'll want to create is the `main.tf`file.

This file is where the connection to your cloud provider is setup, ***(TBC)***


## Terraform Language


## Provisioning AKS Cluster
