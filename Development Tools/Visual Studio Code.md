# Visual Studio Code
---------
***WORK IN PROGRESS***

----

**Table of Content**
- [[#Recommended Plugins]]
- [[#Tips Tricks]]

## Recommended Plugins
To make sure that everyone get the best experience possible with one of the best text-editors out there, I've compiled this list of tools/plugins that I've used over the years. 

The list combines tools that help productivity, as well as quality of life improvements to the existing editor and much more. 

### Quality of Life Plugins
- Color Highlight by Sergii Naumov
- Better Comments by Aaron Bond
- Bracket Pair Colorizer 2 by CoenraadS
- Markdown Preview Enhanced by Yiyi Wang
- Project Manager by Alessandro Fragnani

### Productivity Plugins
- Todo Tree by Gruntfuggly
- LiveShare by Microsoft *(Great for pair programming!)*
- 

### Language Support Plugins
- 

### Themes
As a final bit I just want to recommend a couple of cool themes. The Icon themes especially are great for quickly skimming your heirachy to get an easy overview. 

- 

## Tips & Tricks